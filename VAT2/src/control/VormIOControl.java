package control;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Klasse om bestanden te kunnen openen of opslaan
 */
public class VormIOControl {

	public VormIOControl() {

	}

	/**
	 * Deze methode slaat het object op. Het object wordt in het gewenste
	 * formaat en locatie opgeslagen.
	 * 
	 * @param filenaam
	 *            de naam en filepath waar het object wordt opgeslagen
	 * @param obj
	 *            Het te opslaan object.
	 * @throws IOException
	 *             Wordt gegooid als het fout gaat tijdens het
	 *             opslaan(schrijfrechten bijv.)
	 */
	public void opslaan(String dirName, VormVerzameling obj) throws IOException {
		String naam = dirName + ".dat";
		final ObjectOutputStream save = new ObjectOutputStream(
				new FileOutputStream(naam, true));
		save.writeObject(obj);
		save.flush();
		save.close();
	}

	/**
	 * Deze methode opent de file met het opgeslagen object van hierboven.
	 * 
	 * @param filenaam
	 *            Het gewenste bestand om te openen
	 * @return Geeft een getypecaste VormVerzameling terug
	 * @throws IOException
	 *             Wordt gegooid als er iets fouts gaat tijdens het lezen van
	 *             het bestand
	 * @throws ClassNotFoundException
	 *             Wordt gegooid als de header signatuur niet overeenkomt met
	 *             wat er wordt verwacht door het programma(een VormVerzameling
	 *             in dit geval).
	 */
	public VormVerzameling openen(String filenaam) throws IOException,
			ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(
				filenaam));
		VormVerzameling v = (VormVerzameling) in.readObject();
		in.close();
		return v;
	}
}