package control;

import java.awt.EventQueue;

import view.HoofdFrame;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Applicatie Starter
 */
public class StartApp {

	/**
	 * Start de applicatie. Indien het programma op Mac OS X wordt gedraaid, Zal
	 * het programma er eerst voor zorgen dat de menubar wordt ge�ntregreerd en
	 * dat er bovenaan geen "control.StartApp" komt te staan.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Haalt properties op en controleert zo of het op OSX, Linux of Windows
		// wordt gedraaid
		// Moet voordat er iets wordt geinitialiseerd
		if (System.getProperty("os.name").equals("Mac OS X")) {
			// Set de naam naast het apple logo naar een herkenbare naam.
			System.setProperty(
					"com.apple.mrj.application.apple.menu.about.name", "VAT2");
			// Set het zo dat er geen aparte balk wordt aangemaakt, maar de
			// standaard OSX menubar
			// gebruikt wordt
			System.setProperty("apple.laf.useScreenMenuBar", "true");
		}
		// Laadt het actuele programma
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HoofdFrame frame = new HoofdFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
