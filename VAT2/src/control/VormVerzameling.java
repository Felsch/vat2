package control;

import java.io.Serializable;
import java.util.ArrayList;

import model.Vorm;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een verzameling van alle vormen
 */
public class VormVerzameling implements Serializable {
	private static final long serialVersionUID = 6532715706143374165L;
	private ArrayList<Vorm> vormen;

	public VormVerzameling() {
		vormen = new ArrayList<Vorm>();
	}

	/**
	 * Voegt een nieuwe vorm toe aan de vormen lijst.
	 * 
	 * @param nieuweVorm
	 *            De vorm om toe te voegen.
	 */
	public void voegToe(Vorm nieuweVorm) {
		vormen.add(nieuweVorm);
	}

	/**
	 * Telt de inhoud van alle object in de lijst op.
	 * 
	 * @return Geeft de totale inhoud na berekening terug.
	 */
	public double totaalInhoud() {

		double totaleInhoud = 0;
		for (Vorm v : vormen) {
			totaleInhoud = totaleInhoud + v.berekenInhoud();
		}

		return totaleInhoud;
	}

	/**
	 * Geeft een vorm terug op gewenste index in de vormen lijst.
	 * 
	 * @param index
	 *            de plaats op waar er gezocht moet worden van een vorm
	 * @return de gevonden vorm
	 */
	public Vorm geefVorm(int index) {
		Vorm vorm = vormen.get(index);
		return vorm;
	}

	/**
	 * Verwijdert een vorm uit de vormen lijst op een bepaalde plek.
	 * 
	 * @param index
	 *            De bepaalde plek welke ervoor zorgt dat een voor verwijderd
	 *            kan worden.
	 */
	public void verwijderVorm(int index) {
		vormen.remove(index);
	}

	/**
	 * Haalt de grootte op van de vormenlijst
	 * 
	 * @return de grootte van de vormenlijst
	 */
	public int getSize() {
		return vormen.size();
	}

	/**
	 * Getter om bij de ArrayList te komen
	 * 
	 * @return De ArrayList.
	 */
	public ArrayList<Vorm> getVormen() {
		return vormen;
	}

}