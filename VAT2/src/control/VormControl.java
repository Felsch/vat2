package control;

import java.io.IOException;
import java.util.ArrayList;

import model.Vorm;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Controller klasse, beheert zoveel mogelijk behandelingen tussen view
 *          en model
 */
public class VormControl {
	private VormIOControl vIOC;
	private VormVerzameling v;

	public VormControl() {
		vIOC = new VormIOControl();
	}

	public boolean saveFile(String dirName, VormVerzameling obj) {

		try {
			vIOC.opslaan(dirName, obj);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Geeft de vormverzameling zoals hij is terug. Vooral nodig voor het openen
	 * van een bestand.
	 * 
	 * @return de Vormverzameling.
	 */
	public VormVerzameling getVerzamelingen() {
		return v;
	}

	/**
	 * Geeft een arrayList met Strings terug.
	 * 
	 * @param name
	 *            de naam voor de file die geopent wilt worden
	 * @return De arraylist met strings
	 */
	public ArrayList<String> openFile(String name) {
		ArrayList<String> al = new ArrayList<String>();
		if (checkIfFailToOpen(name)) {
			for (int i = 0; i < v.getSize(); i++) {
				Vorm g = v.geefVorm(i);
				al.add(g.toString());
			}
		} else {
			// TODO Verzin hier iets...
		}

		return al;
	}

	/**
	 * Deze private methode controleert of bestand wel geopent is en of
	 * excepties zijn.
	 * 
	 * @param name
	 *            de naam van het bestand dat je wilt openen
	 * @return een true als het gelukt is, een false als het niet gelukt is.
	 */
	private boolean checkIfFailToOpen(String name) {

		try {
			v = vIOC.openen(name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
