package control;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Deze klasse beheert de extensie die gebruikt wordt tijdens het
 *          opslaan of openen van een bestand. Dat zorgt ervoor dat je minder
 *          hoeft te zoeken naar een bepaalde extensie of bestand met die
 *          extensie. Voor de VAT2 is de standaard extensie ".dat".
 */
public class FileDataFilter extends FileFilter {
	private String DataFormat = "dat";
	private char DotIndex = '.';

	public FileDataFilter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		if (extension(f).equalsIgnoreCase(DataFormat)) {
			return true;
		} else
			return false;
	}

	@Override
	public String getDescription() {
		return "VAT Data Files";
	}

	public String extension(File f) {
		String FileName = f.getName();
		int IndexFile = FileName.lastIndexOf(DotIndex);
		if (IndexFile > 0 && IndexFile < FileName.length() - 1) {
			return FileName.substring(IndexFile + 1);
		} else {
			return "";
		}
	}

}
