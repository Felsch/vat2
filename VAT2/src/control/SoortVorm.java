package control;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een enum die voor alle vormen een waarde heeft. Deze enum zorgt voor
 *          de populatie van de combobox en daarmee is het makkelijker en
 *          sneller om nieuwere vormen toe te voegen.
 * 
 */
public enum SoortVorm {

	VORM_BOL("Bol"), VORM_BLOK("Blok"), VORM_CILLINDER("Cillinder");

	private final String naam;

	public String toString() {
		return naam;
	}

	SoortVorm(String naam) {
		this.naam = naam;
	}
}
