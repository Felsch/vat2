package model;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een cilinder.
 */
public class Cilinder extends Vorm {
	private static final long serialVersionUID = 2081655619191706471L;
	private int hoogte;
	private int straal;

	public Cilinder(int hoogte, int straal) {
		this.hoogte = hoogte;
		this.straal = straal;
	}

	/**
	 * Berekent de inhoud van een cillinder d.m.v. pi*R^2*H
	 */
	@Override
	public double berekenInhoud() {
		inhoud = super.inhoud;

		double pi = Math.PI;
		double s = Math.pow(getStraal(), 2);
		double h = getHoogte();

		inhoud = pi * s * h;

		return inhoud;
	}

	/**
	 * Geeft de string waarde terug als er wordt gevraagd voor een cillinder.
	 * Geeft tevens de straal en hoogte mee
	 */
	@Override
	public String toString() {
		return "Cillinder " + String.valueOf(getStraal()) + " "
				+ String.valueOf(getHoogte());
	}

	/**
	 * Getters en setters
	 */

	public int getHoogte() {
		return hoogte;
	}

	public void setHoogte(int hoogte) {
		this.hoogte = hoogte;
	}

	public int getStraal() {
		return straal;
	}

	public void setStraal(int straal) {
		this.straal = straal;
	}

}