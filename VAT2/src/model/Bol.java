package model;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een bol.
 */
public class Bol extends Vorm {
	private static final long serialVersionUID = 6693544723757146694L;
	public int straal;

	public Bol(int straal) {
		this.straal = straal;
	}

	/**
	 * Berekent de inhoud van de bol d.m.v. V = (4/3)*pi*R^3
	 */
	@Override
	public double berekenInhoud() {
		inhoud = super.inhoud;

		double i = 4.0 / 3.0;
		double pi = Math.PI;
		double s = Math.pow(getStraal(), 3);

		inhoud = i * pi * s;

		return inhoud;
	}

	/**
	 * Geeft de string waarde terug van een bol als er om gevraagd wordt Geeft
	 * tevens de waarde van de straal mee
	 */
	@Override
	public String toString() {
		return "Bol " + String.valueOf(getStraal());
	}

	/**
	 * Getters en setters
	 */

	public int getStraal() {
		return straal;
	}

	public void setStraal(int straal) {
		this.straal = straal;
	}

}