package model;

import java.io.Serializable;

/**
 * @author felsch
 * @version 1.0
 * 
 *          De abstracte klasse vorm. Deze klasse is de basis voor alle vormen
 *          en alle abstracte methodes die deze klasse bevat, zal worden
 *          overgenomen door de subklasses
 */
public abstract class Vorm implements Serializable {
	private static final long serialVersionUID = -5888706668650600464L;
	protected double inhoud;

	public abstract double berekenInhoud();

	public abstract String toString();

	/**
	 * Getter; geeft de inhoud terug, om deze vervolgens te laten zien om het
	 * hoofdscherm
	 * 
	 * @return inhoud de inhoud van 1 vorm.
	 */
	public double getInhoud() {
		return inhoud;
	}

}