package model;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een blok.
 */
public class Blok extends Vorm {

	private static final long serialVersionUID = 3922039159713854061L;
	private int lengte;
	private int breedte;
	private int hoogte;

	public Blok(int lengte, int breedte, int hoogte) {
		super();
		this.lengte = lengte;
		this.breedte = breedte;
		this.hoogte = hoogte;
	}

	/**
	 * Berekent de inhoud van een blok d.m.v. V = L*B*H.
	 */
	@Override
	public double berekenInhoud() {
		// inhoud = super.inhoud;

		int l = getLengte();
		int b = getBreedte();
		int h = getHoogte();

		inhoud = l * b * h;
		return inhoud;
	}

	/**
	 * String waarde van een blok. Geeft tevens de lengte, hoogte en breedte
	 * mee.
	 */
	@Override
	public String toString() {
		return "Blok " + String.valueOf(getLengte()) + " "
				+ String.valueOf(getBreedte()) + " "
				+ String.valueOf(getHoogte());
	}

	/**
	 * Getters en Setters
	 */
	public int getLengte() {
		return lengte;
	}

	public void setLengte(int lengte) {
		this.lengte = lengte;
	}

	public int getBreedte() {
		return breedte;
	}

	public void setBreedte(int breedte) {
		this.breedte = breedte;
	}

	public int getHoogte() {
		return hoogte;
	}

	public void setHoogte(int hoogte) {
		this.hoogte = hoogte;
	}
}