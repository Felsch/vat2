package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.Blok;
import model.Vorm;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een frame om gegevens van een blok in te voeren. Na het invoeren
 *          kunnen de gegevens worden gebruikt om de inhoud en totale inhoud te
 *          berekenen.
 */
public final class BlokFrame extends HoofdFrame {

	private static final long serialVersionUID = -1898196204125832861L;
	private JPanel contentPane;
	private JTextField lengte_textField;
	private JTextField breedte_textField_1;
	private JTextField hoogte_textField_2;

	/**
	 * Create the frame.
	 */
	public BlokFrame(final HoofdFrame f) {
		setJMenuBar(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Bereken inhoud van een blok");
		setBounds(100, 100, 289, 192);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblVoegDeGegevens = new JLabel(
				"Voeg de gegevens van een blok toe:");
		lblVoegDeGegevens.setBounds(12, 12, 268, 15);
		contentPane.add(lblVoegDeGegevens);

		JLabel lblLengte = new JLabel("Lengte:");
		lblLengte.setBounds(12, 52, 70, 15);
		contentPane.add(lblLengte);

		JLabel lblBreda = new JLabel("Breedte:");
		lblBreda.setBounds(12, 78, 70, 15);
		contentPane.add(lblBreda);

		JLabel lblHoogte = new JLabel("Hoogte:");
		lblHoogte.setBounds(12, 105, 70, 15);
		contentPane.add(lblHoogte);

		lengte_textField = new JTextField();
		lengte_textField.setBounds(87, 50, 184, 19);
		contentPane.add(lengte_textField);
		lengte_textField.setColumns(10);

		breedte_textField_1 = new JTextField();
		breedte_textField_1.setBounds(87, 76, 184, 19);
		contentPane.add(breedte_textField_1);
		breedte_textField_1.setColumns(10);

		hoogte_textField_2 = new JTextField();
		hoogte_textField_2.setBounds(87, 103, 184, 19);
		contentPane.add(hoogte_textField_2);
		hoogte_textField_2.setColumns(10);

		/**
		 * Door te klikken op deze button, worden de waardes naar int's
		 * geparsed, wordt er een nieuwe vorm object van type blok gemaakt en
		 * wordt deze toegevoegd aan de lijsten(DefaultModelList & de
		 * ArrayList). Na uitvoering word het scherm gesloten.
		 */
		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					/**
					 * if-else contstructie, omdat een numberformatexception en
					 * niks invullen niet het zelfde zijn
					 */
					if (!lengte_textField.getText().isEmpty()
							|| !breedte_textField_1.getText().isEmpty()
							|| !hoogte_textField_2.getText().isEmpty()) {
						int l = Integer.parseInt(lengte_textField.getText());
						int b = Integer.parseInt(breedte_textField_1.getText());
						int h = Integer.parseInt(hoogte_textField_2.getText());
						Vorm blok = new Blok(l, b, h);
						blok.berekenInhoud();
						String blo = blok.toString();
						verzameling.voegToe(blok);
						f.updateList(blo);
						setVisible(false);
					} else {
						JOptionPane.showMessageDialog(null,
								"Alles moet ingevuld zijn!");
					}
				} catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null,
							"Er MOETEN nummers ingevoerd worden!");
					nfe.printStackTrace();
				}

			}
		});
		btnToevoegen.setBounds(154, 128, 117, 25);
		contentPane.add(btnToevoegen);
		setVisible(true);
	}
}