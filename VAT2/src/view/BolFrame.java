package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.Bol;
import model.Vorm;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een frame om gegevens van een bol in te voeren. Na het invoeren
 *          kunnen de gegevens worden gebruikt om de inhoud en totale inhoud te
 *          berekenen.
 */
public class BolFrame extends HoofdFrame {

	private static final long serialVersionUID = -4207135683583564761L;
	private JPanel contentPane;
	private JTextField straal_TextField;

	/**
	 * Create the frame.
	 */
	public BolFrame(final HoofdFrame f) {
		setJMenuBar(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Bereken de inhoud van een bol");
		setBounds(100, 100, 283, 136);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblVoegDeGegevens = new JLabel(
				"Voeg de gegevens toe van een bol:");
		lblVoegDeGegevens.setBounds(12, 12, 250, 15);
		contentPane.add(lblVoegDeGegevens);

		JLabel lblStraal = new JLabel("Straal:");
		lblStraal.setBounds(12, 45, 54, 15);
		contentPane.add(lblStraal);

		straal_TextField = new JTextField();
		straal_TextField.setBounds(65, 43, 197, 19);
		contentPane.add(straal_TextField);
		straal_TextField.setColumns(10);

		/**
		 * Door te klikken op deze button, worden de waardes naar int's
		 * geparsed, wordt er een nieuwe vorm object van type Bol gemaakt en
		 * wordt deze toegevoegd aan de lijsten(DefaultModelList & de
		 * ArrayList). Na uitvoering word het scherm gesloten.
		 */
		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					/**
					 * if-else contstructie, omdat een numberformatexception en
					 * niks invullen niet het zelfde zijn
					 */
					if (!straal_TextField.getText().isEmpty()) {
						int s = Integer.parseInt(straal_TextField.getText());
						Vorm bol = new Bol(s);
						bol.berekenInhoud();
						String bo = bol.toString();
						verzameling.voegToe(bol);
						f.updateList(bo);
						setVisible(false);
					} else {
						JOptionPane.showMessageDialog(null,
								"Alles moet ingevuld zijn!");
					}
				} catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null,
							"Er MOETEN nummers ingevoerd worden!");
					nfe.printStackTrace();
				}
			}
		});
		btnToevoegen.setBounds(148, 74, 117, 25);
		contentPane.add(btnToevoegen);
		setVisible(true);
	}
}