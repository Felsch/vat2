package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class About extends HoofdFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3771032560206247787L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public About() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 216, 196);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblVat = new JLabel("Vorm Analyse Tool 2");
		lblVat.setBounds(5, 5, 215, 16);
		contentPane.add(lblVat);
		
		JLabel lblVersie = new JLabel("Versie 1.0");
		lblVersie.setBounds(5, 33, 68, 16);
		contentPane.add(lblVersie);
		
		JLabel lblGemaaktDoorRavi = new JLabel("Gemaakt door: Ravi Rampersad");
		lblGemaaktDoorRavi.setBounds(6, 61, 202, 16);
		contentPane.add(lblGemaaktDoorRavi);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnOk.setBounds(93, 117, 117, 29);
		contentPane.add(btnOk);
		
		JLabel lblVersionOnGit = new JLabel("Version on Git source control");
		lblVersionOnGit.setBounds(5, 89, 208, 16);
		contentPane.add(lblVersionOnGit);
	}
}
