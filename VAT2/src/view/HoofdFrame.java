package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import control.FileDataFilter;
import control.VormControl;
import control.VormVerzameling;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import model.Vorm;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * @author felsch
 * @version 1.0
 * 
 *          De Frame die je altijd ziet en pas weg zal gaan als je de applicatie
 *          sluit.
 */
public class HoofdFrame extends JFrame {
	private static final long serialVersionUID = -6095207781685890748L;
	private JPanel contentPane;
	private JLabel inhoudVorm;
	private JLabel inhoudLabel;
	protected static VormVerzameling verzameling = new VormVerzameling();
	private VormControl controle;
	private DefaultListModel model;
	private JFileChooser dialog;
	private JList list;
	private FileDataFilter filter;
	private DecimalFormat df = new DecimalFormat("#.##");
	private ImageIcon image;

	/**
	 * Create the frame.
	 */
	public HoofdFrame() {
		filter = new FileDataFilter();
		controle = new VormControl();
		dialog = new JFileChooser();
		setTitle("Vorm Analyse Tool - Ravi Rampersad - Studentnummer 2019032 - 09-2012");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 286);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnBestand = new JMenu("Bestand");
		menuBar.add(mnBestand);

		JMenuItem mntmVormlijstLaden = new JMenuItem("Vormlijst Laden");
		mntmVormlijstLaden.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				InputEvent.CTRL_MASK));
		mntmVormlijstLaden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openDialog();
			}
		});
		mnBestand.add(mntmVormlijstLaden);

		JMenuItem mntmVormlijstOpslaan = new JMenuItem("Vormlijst Opslaan");
		mntmVormlijstOpslaan.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmVormlijstOpslaan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveDialog();
			}
		});
		mnBestand.add(mntmVormlijstOpslaan);

		Component verticalStrut_1 = Box.createVerticalStrut(3);
		mnBestand.add(verticalStrut_1);

		JSeparator separator = new JSeparator();
		mnBestand.add(separator);

		Component verticalStrut = Box.createVerticalStrut(3);
		mnBestand.add(verticalStrut);

		JMenuItem mntmAfsluiten = new JMenuItem("Afsluiten");
		mntmAfsluiten.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				InputEvent.CTRL_MASK));
		mntmAfsluiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnBestand.add(mntmAfsluiten);

		JMenu mnInfo = new JMenu("Info");
		menuBar.add(mnInfo);

		JMenuItem mntmOver = new JMenuItem("Over");
		mntmOver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About aFrame = new About();
				aFrame.setVisible(true);
			}
		});

		mnInfo.add(mntmOver);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblIkWilDe = new JLabel("Vorm toevoegen:");
		lblIkWilDe.setBounds(12, 12, 129, 15);
		contentPane.add(lblIkWilDe);

		final JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int boxitem = comboBox.getSelectedIndex();
				if (boxitem != -1) {
					changeComboBoxItem(boxitem);
					inhoudVorm.setText("0.0");
					inhoudLabel.setText(String.valueOf(df.format(verzameling
							.totaalInhoud())));
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(control.SoortVorm.values()));
		comboBox.setSelectedIndex(-1);
		comboBox.setBounds(12, 39, 176, 24);
		contentPane.add(comboBox);
		
		image = new ImageIcon(getClass().getResource("/resources/ZeroZFive.png"));
		JLabel imageLabel = new JLabel(image);
		imageLabel.setBounds(30, 75, image.getIconWidth(), image.getIconHeight());
		contentPane.add(imageLabel);

		JLabel lblTotaleInhoud = new JLabel("Totale inhoud:");
		lblTotaleInhoud.setBounds(13, 213, 103, 15);
		contentPane.add(lblTotaleInhoud);

		inhoudLabel = new JLabel("0.0");
		inhoudLabel.setBounds(128, 214, 75, 15);
		contentPane.add(inhoudLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(221, 12, 176, 185);
		contentPane.add(scrollPane);

		/**
		 * Een DefaultListModel welke elementen bevatten die ook in de ArrayList
		 * van VormVerzameling terugkomen. De Jlist heeft een
		 * ListSelectionListener welke luistert of de waarde is veranderd.
		 */

		model = new DefaultListModel();
		list = new JList(model);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int i = list.getSelectedIndex();
				try {
					if (i != -1) {
						Vorm v2 = verzameling.geefVorm(i);
						for (Vorm v : verzameling.getVormen())
							if (v2 != null && v2 == v) {
								v.toString();

								inhoudVorm.setText(String.valueOf(df.format(v
										.getInhoud())));
								inhoudLabel.setText(String.valueOf(df
										.format(verzameling.totaalInhoud())));
							}
					}
				} catch (IndexOutOfBoundsException ioobe) {
					JOptionPane.showMessageDialog(null,
							"Er is niks om te selecteren :(");
					ioobe.printStackTrace();
				}

			}
		});
		scrollPane.setViewportView(list);

		JLabel lblInhoud = new JLabel("Inhoud:");
		lblInhoud.setBounds(13, 186, 70, 15);
		contentPane.add(lblInhoud);

		inhoudVorm = new JLabel("0.0");
		inhoudVorm.setBounds(127, 186, 76, 15);
		contentPane.add(inhoudVorm);

		/**
		 * Verwijdert een element uit de jList en ArrayList op de bepaalde
		 * index. De index moet groter dan 0 zijn! Vernieuwt ook de inhoud en
		 * totale inhoud.
		 */
		JButton btnVerwijderVorm = new JButton("Verwijder Vorm\n");
		btnVerwijderVorm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = list.getSelectedIndex();
				try {
					if (i >= 0) {
						Vorm v2 = verzameling.geefVorm(i);
						if (v2 != null) {
							model.removeElementAt(i);
							verzameling.verwijderVorm(i);
						}
					} else {
						JOptionPane.showMessageDialog(null,
								"Er is niks geselecteerd!");
					}
					inhoudVorm.setText(df.format(0));
					inhoudLabel.setText(String.valueOf(df.format(verzameling
							.totaalInhoud())));
				} catch (IndexOutOfBoundsException ioobe) {
					JOptionPane.showMessageDialog(null,
							"Er staat niks in de lijst om te verwijderen");
					ioobe.printStackTrace();
				}

			}

		});
		btnVerwijderVorm.setBounds(221, 204, 176, 25);
		contentPane.add(btnVerwijderVorm);
	}

	/**
	 * Gebaseerd op welke box item hij binnen krijgt, opent deze methode een
	 * nieuw scherm.
	 * 
	 * @param boxitem
	 *            de selectedindex verkregen van de combobox
	 */
	public void changeComboBoxItem(int boxitem) {

		switch (boxitem) {
		case 0:
			new BolFrame(this);
			break;
		case 1:
			new BlokFrame(this);
			break;
		case 2:
			new CilinderFrame(this);
			break;
		default:
			JOptionPane.showMessageDialog(null, "Er is niks geselecteerd!");
		}
	}

	/**
	 * Voegt de item toe aan de jList en vernieuwt de inhoudLabel. Set de
	 * inhoudVorm op 0.(weer staat mooier dan niets).
	 * 
	 * @param item
	 *            de toString methode van verschillende vormen, om deze
	 *            makkelijk aan de list toe te voegen.
	 */
	public void updateList(String item) {
		model.addElement(item);

		inhoudVorm.setText(df.format(0));
		inhoudLabel
				.setText(String.valueOf(df.format(verzameling.totaalInhoud())));

	}

	/**
	 * Laat een save dialog zien, waarna als er op save gedrukt wordt ook
	 * daadwerkelijk wordt gesaved. Je kan alleen opslaan als ".dat".
	 */
	private void saveDialog() {
		dialog.setFileFilter(filter);
		int saveChoice = dialog.showSaveDialog(this);

		if (saveChoice == JFileChooser.APPROVE_OPTION) {

			File saveFile = dialog.getSelectedFile();
			String dir = saveFile.getPath();
			boolean b = (boolean) controle.saveFile(dir, verzameling);

			if (b != true) {
				JOptionPane
						.showMessageDialog(null,
								"Er is iets misgegaan met het opslaan, probeer het a.u.b. opnieuw!");
			}
		}
	}

	/**
	 * Laat een open dialog zien. Je kan alleen ".dat" bestanden openen(of VAT
	 * Data Files is hoe ik het heb genoemd). Voegt vervolgens beide toe aan de
	 * lijst verzamelingen en de DefaultListModel.
	 */
	private void openDialog() {
		dialog.setFileFilter(filter);
		int openChoice = dialog.showOpenDialog(this);

		if (openChoice == JFileChooser.APPROVE_OPTION) {

			File openFile = dialog.getSelectedFile();
			ArrayList<String> s = controle.openFile(openFile.getPath());
			for (String st : s) {
				updateList(st);
			}
			/**
			 * Haal de verzameling op van de controller, itereer vervolgens door
			 * de verzameling en voeg deze toe aan een al bestaande lijst.
			 * Voorkomt dat je een melding krijgt dat er niks geselecteerd is.
			 */
			VormVerzameling ver = controle.getVerzamelingen();
			for (Vorm vm : ver.getVormen()) {
				verzameling.voegToe(vm);
			}
		}
	}
}