package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.Cilinder;
import model.Vorm;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author felsch
 * @version 1.0
 * 
 *          Een frame om gegevens van een cilinder in te voeren. Na het invoeren
 *          kunnen de gegevens worden gebruikt om de inhoud en totale inhoud te
 *          berekenen.
 */
public class CilinderFrame extends HoofdFrame {

	private static final long serialVersionUID = 530290902325291591L;
	private JPanel contentPane;
	private JTextField straal_textField;
	private JTextField hoogte_textField_1;

	/**
	 * Create the frame.
	 */
	public CilinderFrame(final HoofdFrame f) {
		setJMenuBar(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Bereken inhoud van een cillinder");
		setBounds(100, 100, 311, 156);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblVoegDeGegevens = new JLabel(
				"Voeg de gegevens toe van een cillinder:");
		lblVoegDeGegevens.setBounds(12, 12, 284, 15);
		contentPane.add(lblVoegDeGegevens);

		JLabel lblStraal = new JLabel("Straal:");
		lblStraal.setBounds(12, 39, 48, 15);
		contentPane.add(lblStraal);

		JLabel lblHoogte = new JLabel("Hoogte:");
		lblHoogte.setBounds(12, 66, 57, 15);
		contentPane.add(lblHoogte);

		straal_textField = new JTextField();
		straal_textField.setBounds(78, 39, 218, 19);
		contentPane.add(straal_textField);
		straal_textField.setColumns(10);

		hoogte_textField_1 = new JTextField();
		hoogte_textField_1.setBounds(78, 64, 218, 19);
		contentPane.add(hoogte_textField_1);
		hoogte_textField_1.setColumns(10);

		/**
		 * Door te klikken op deze button, worden de waardes naar int's
		 * geparsed, wordt er een nieuwe vorm object van type cillinder gemaakt
		 * en wordt deze toegevoegd aan de lijsten(DefaultModelList & de
		 * ArrayList). Na uitvoering word het scherm gesloten.
		 */
		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					/**
					 * if-else contstructie, omdat een numberformatexception en
					 * niks invullen niet het zelfde zijn
					 */
					if (!straal_textField.getText().isEmpty()
							|| !hoogte_textField_1.getText().isEmpty()) {
						int s = Integer.parseInt(straal_textField.getText());
						int h = Integer.parseInt(hoogte_textField_1.getText());
						Vorm cilinder = new Cilinder(s, h);
						cilinder.berekenInhoud();
						String cil = cilinder.toString();
						verzameling.voegToe(cilinder);
						f.updateList(cil);
						setVisible(false);
					} else {
						// Voor als er niks is gevuld
						JOptionPane.showMessageDialog(null,
								"Alles moet ingevuld zijn!");
					}
				} catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null,
							"Er kunnen alleen nummers worden ingevoerd!");
					nfe.printStackTrace();
				}
			}
		});

		btnToevoegen.setBounds(179, 95, 117, 25);
		contentPane.add(btnToevoegen);
		setVisible(true);
	}

}