Deze applicatie is een simpele schoolopdracht die er voor zorgt dat je inhouden kunt berekenen van een paar vormen. Een simpele opdracht inprincipe.

Als je iets van deze opdracht wilt gebruiken, prima, maar ik hoop eigenlijk meer dat je er wat van leert :).

------------------

English:

This application is a simple school exercise. The exercise calculates the Volumes of certains vorms. It's a really simple exercise.

If you want to use something of this code, fine, but I really hope you learn something from it, rather then just taking it :).

Also, every java-doc is in dutch, so don't be scared if you can't read that :)