package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class BolTest {

	@Test
	public void testBerekenInhoud() {
		double straal = 5;
		//int vier = 4;
		//int drie = 3;
		
		double expected = (4.0/3.0)*Math.PI*(Math.pow(straal, 3));
		double actual = 523.598775598; 
		
		assertEquals(expected, actual, 0.01);
	}

}
